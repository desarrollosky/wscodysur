'use strict';

const mysql = require('mysql');

var connection = mysql.createConnection({
    host     : 'localhost',
    port     : '3307',
    user     : 'root', 
    password : 'admin',
    database : 'nuevogestor_codysur',
    dateStrings: 'date',
    multipleStatements: true
});

connection.connect(function(err) {
    if (err) {
        console.error(err.message);
    } else {
        console.log('Connection success');
    }
});

module.exports = connection;