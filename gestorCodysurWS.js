const connection = require('./db.js');
const { wialon } = require('./wialon-node2.js');
let request = require('request');
const express = require('express');
const fetch = require('node-fetch');
var moment = require('moment');

const app = express();

var sess;
var actualSess;

//User: codysurgroup1
//Pass: 123456

getNotificaciones();

function getNotificaciones() {
    actualSess = wialon.core.Session.getInstance();
    actualSess.initSession("https://hst-api.wialon.com");
    var token = "242c6d0a408aeefb009d4e76f33922534FB00916568273DC906FD794E3FE7D5AE0367C24";
    var account = "codysurgroup1";
    actualSess.loginToken(token, account,
        function (code) {
            if (code) {
                console.log(wialon.core.Errors.getErrorText(code));
                return;
            }
            console.log("Logged successfully");
            traerNotificaciones();
        });
}

function traerNotificaciones() {
    sess = wialon.core.Session.getInstance();
    var flags = wialon.item.Item.dataFlag.base | wialon.item.Resource.dataFlag.base | wialon.item.Item.dataFlag.messages | wialon.item.Resource.dataFlag.notifications;

    sess.loadLibrary("resourceNotifications");

    sess.updateDataFlags( // load items to current session
        [{
            type: "type",
            data: "avl_resource",
            flags: flags,
            mode: 1
        },
        {
            type: "type",
            data: "avl_unit",
            flags: flags,
            mode: 0
        },
        {
            type: "type",
            data: "avl_unit_group",
            flags: flags,
            mode: 0
        }
        ],
        function (code) {
            if (code) {
                console.log(wialon.core.Errors.getErrorText(code));
                return;
            }

            var res = sess.getItems("avl_resource");

            for (var i = 0; i < res.length; i++) { // construct Select list using found resources
                res[i].addListener("messageRegistered", showData); // register event when we will receive message
            }
        });
}

function showData(event) {

    var grupos = "";
    var modulos = "";
    var data = event.getData();


        if (data.tp && data.tp == "unm") {
            var auxCaracter = data.name;

            var auxval = auxCaracter.indexOf("~", auxCaracter.length - 1);
            if (auxval != -1) {

                console.log(data);

                var unit = wialon.core.Session.getInstance().getItem(data.unit);
                var pos = unit.getPosition();
                if (pos) {
                    wialon.util.Gis.getLocations([{ lon: pos.x, lat: pos.y }], function (code, address) {
                        if (code) {
                            console.log(wialon.core.Errors.getErrorText(code));
                            return;
                        }
                    });
                }

                var units = sess.getItems("avl_unit_group");
                for (var i = 0; i < units.length; i++) {
                    var u = units[i]; // current unit in cycle
                    if (!u.getName().indexOf("z") || !u.getName().indexOf("#")) {

                    } else {
                        for (var j = 0; j < u.getUnits().length; j++) {
                            if (u.getUnits()[j] === unit.getId()) {
                                if (u.getName().indexOf("mod")) {

                                    grupos = grupos.concat(u.getName(), ",");
                                } else {
                                    modulos = modulos.concat(u.getName(), ",");
                                }
                            }
                        }
                    }
                }


                let name = data.name;
                let lon = data.y;
                let lat = data.x;
                let unidad = unit.getName();
                let groups = grupos;
                let mod = modulos;
                //let timeNot = data.t;
                //let fecha = moment.unix(timeNot).format('YYYY-MM-DD HH:mm:ss');

                var hoy = new Date();
                var dia = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();
                var hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
                var fecha = dia + " " + hora;

                connection.query("SELECT * FROM notificacion WHERE fecha=? AND idUnidad=? AND nombre=?;", [fecha, unidad, name], function (err, rows, fields) {
                    if (err) { throw err; } else {
                        if (rows.length == 0) {
                            connection.query("INSERT INTO notificacion(idUnidad, estado, fecha, nombre, texto, latitude, longitude, grupo) VALUES(?, ?, ?, ?, ?, ?, ?, ?);", [unidad, '1', fecha, name, mod, lat, lon, groups], function (err, rows, fields) {
                                if (err) { throw err; }
                                else {
                                    console.log('Insert Notification: OK:::::');
                                }
                            });
                        }
                    }
                });
            }
        }

}